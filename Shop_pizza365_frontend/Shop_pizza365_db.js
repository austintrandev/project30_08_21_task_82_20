/**
 * CƠ SỞ DỮ LIỆU SHOP365_PIZZA
*/

// Khởi tạo mảng gCustomerDb gán dữ liêu từ server trả về 
var gCustomerDb = {
  customers: [],

  // customer methods
  // function get Customer index form Customer id
  // get Customer index from Customer id
  getIndexFromCustomerId: function (paramCustomerId) {
    var vCustomerIndex = -1;
    var vCustomerFound = false;
    var vLoopIndex = 0;
    while (!vCustomerFound && vLoopIndex < this.customers.length) {
      if (this.customers[vLoopIndex].id === paramCustomerId) {
        vCustomerIndex = vLoopIndex;
        vCustomerFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vCustomerIndex;
  },

  // hàm show customer obj lên form
  showCustomerDataToForm: function (paramRequestData) {

    $("#inp-first-name").val(paramRequestData.firstName);
    $("#inp-last-name").val(paramRequestData.lastName);
    $("#inp-phone-number").val(paramRequestData.phoneNumber);
    $("#inp-address").val(paramRequestData.address);
    $("#inp-city").val(paramRequestData.city);
    $("#inp-state").val(paramRequestData.state);
    $("#inp-country").val(paramRequestData.country);
    $("#inp-postal-code").val(paramRequestData.postalCode);
    $("#inp-sales-rep-employee-number").val(paramRequestData.salesRepEmployeeNumber);
    $("#inp-credit-limit").val(paramRequestData.creditLimit);
  },

};

// Khởi tạo mảng gPaymentDb gán dữ liêu từ server trả về 
var gPaymentDb = {
  payments: [],

  // payment methods
  // function get payment index form payment id
  // get payment index from payment id
  getIndexFromPaymentId: function (paramPaymentId) {
    var vPaymentIndex = -1;
    var vPaymentFound = false;
    var vLoopIndex = 0;
    while (!vPaymentFound && vLoopIndex < this.payments.length) {
      if (this.payments[vLoopIndex].id === paramPaymentId) {
        vPaymentIndex = vLoopIndex;
        vPaymentFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vPaymentIndex;
  },

  // hàm show payment obj lên form
  showPaymentDataToForm: function (paramRequestData) {
    $("#inp-check-number").val(paramRequestData.checkNumber);
    $("#inp-ammount").val(paramRequestData.ammount);
    $("#inp-payment-date").find("input").val(paramRequestData.paymentDate);
  },

 
};
 // Khởi tạo mảng gCheckAllPaymentDb gán dữ liêu từ server trả về để check dữ liệu trùng all payment 
 var gCheckAllPaymentDb = {
  payments: [],
}

// Khởi tạo mảng gOrderDb gán dữ liêu từ server trả về 
var gOrderDb = {
  orders: [],

  // order methods
  // function get order index form order id
  // get order index from order id
  getIndexFromOrderId: function (paramOrderId) {
    var vOrderIndex = -1;
    var vOrderFound = false;
    var vLoopIndex = 0;
    while (!vOrderFound && vLoopIndex < this.orders.length) {
      if (this.orders[vLoopIndex].id === paramOrderId) {
        vOrderIndex = vLoopIndex;
        vOrderFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vOrderIndex;
  },

  // hàm show order obj lên form
  showOrderDataToForm: function (paramRequestData) {
    $("#inp-status").val(paramRequestData.status);
    $("#inp-comments").val(paramRequestData.comments);
    $("#inp-order-date").find("input").val(paramRequestData.orderDate);
    $("#inp-required-date").find("input").val(paramRequestData.requiredDate);
    $("#inp-shipped-date").find("input").val(paramRequestData.shippedDate);
  },

 
};


// Khởi tạo mảng gOrderDetailDb gán dữ liêu từ server trả về 
var gOrderDetailDb = {
  orderdetails: [],

  // orderdetail methods
  // function get orderdetail index form orderdetail id
  // get orderdetail index from orderdetail id
  getIndexFromOrderDetailId: function (paramOrderDetailId) {
    var vOrderDetailIndex = -1;
    var vOrderDetailFound = false;
    var vLoopIndex = 0;
    while (!vOrderDetailFound && vLoopIndex < this.orderdetails.length) {
      if (this.orderdetails[vLoopIndex].id === paramOrderDetailId) {
        vOrderDetailIndex = vLoopIndex;
        vOrderDetailFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vOrderDetailIndex;
  },

  // hàm show orderdetail obj lên form
  showOrderDetailDataToForm: function (paramRequestData) {
    $("#inp-quantity-order").val(paramRequestData.quantityOrder);
    $("#inp-price-each").val(paramRequestData.priceEach);
  },

 
};


// Khởi tạo mảng gProductDb gán dữ liêu từ server trả về 
var gProductDb = {
  products: [],

  // product methods
  // function get product index form product id
  // get product index from product id
  getIndexFromProductId: function (paramProductId) {
    var vProductIndex = -1;
    var vProductFound = false;
    var vLoopIndex = 0;
    while (!vProductFound && vLoopIndex < this.products.length) {
      if (this.products[vLoopIndex].id === paramProductId) {
        vProductIndex = vLoopIndex;
        vProductFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vProductIndex;
  },

  // hàm show product obj lên form
  showProductDataToForm: function (paramRequestData) {

    $("#inp-product-code").val(paramRequestData.productCode);
    $("#inp-product-name").val(paramRequestData.productName);
    $("#inp-product-description").val(paramRequestData.productDescription);
    $("#inp-product-scale").val(paramRequestData.productScale);
    $("#inp-product-vendor").val(paramRequestData.productVendor);
    $("#inp-quantity-in-stock").val(paramRequestData.quantityInStock);
    $("#inp-buy-price").val(paramRequestData.buyPrice);
  },

};

// Khởi tạo mảng gCheckAllProductDb gán dữ liêu từ server trả về để check dữ liệu trùng all product 
var gCheckAllProductDb = {
  products: [],
}

// Khởi tạo mảng gProductLineDb gán dữ liêu từ server trả về 
var gProductLineDb = {
  productlines: [],

  // productline methods
  // function get productline index form productline id
  // get productline index from productline id
  getIndexFromProductLineId: function (paramProductLineId) {
    var vProductLineIndex = -1;
    var vProductLineFound = false;
    var vLoopIndex = 0;
    while (!vProductLineFound && vLoopIndex < this.productlines.length) {
      if (this.productlines[vLoopIndex].id === paramProductLineId) {
        vProductLineIndex = vLoopIndex;
        vProductLineFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vProductLineIndex;
  },

  // hàm show productline obj lên form
  showProductLineDataToForm: function (paramRequestData) {

    $("#inp-product-line").val(paramRequestData.productLine);
    $("#inp-description").val(paramRequestData.description);
  },

};

// Khởi tạo mảng gEmployeeDb gán dữ liêu từ server trả về 
var gEmployeeDb = {
  employees: [],

  // employee methods
  // function get employee index form employee id
  // get employee index from employee id
  getIndexFromEmployeeId: function (paramEmployeeId) {
    var vEmployeeIndex = -1;
    var vEmployeeFound = false;
    var vLoopIndex = 0;
    while (!vEmployeeFound && vLoopIndex < this.employees.length) {
      if (this.employees[vLoopIndex].id === paramEmployeeId) {
        vEmployeeIndex = vLoopIndex;
        vEmployeeFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vEmployeeIndex;
  },

  // hàm show employee obj lên form
  showEmployeeDataToForm: function (paramRequestData) {

    $("#inp-first-name").val(paramRequestData.firstName);
    $("#inp-last-name").val(paramRequestData.lastName);
    $("#inp-extension").val(paramRequestData.extension);
    $("#inp-email").val(paramRequestData.email);
    $("#inp-office-code").val(paramRequestData.officeCode);
    $("#inp-report-to").val(paramRequestData.reportTo);
    $("#inp-job-title").val(paramRequestData.jobTitle);
  },

};

// Khởi tạo mảng gOfficeDb gán dữ liêu từ server trả về 
var gOfficeDb = {
  offices: [],

  // office methods
  // function get office index form office id
  // get office index from office id
  getIndexFromOfficeId: function (paramOfficeId) {
    var vOfficeIndex = -1;
    var vOfficeFound = false;
    var vLoopIndex = 0;
    while (!vOfficeFound && vLoopIndex < this.offices.length) {
      if (this.offices[vLoopIndex].id === paramOfficeId) {
        vOfficeIndex = vLoopIndex;
        vOfficeFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vOfficeIndex;
  },

  // hàm show office obj lên form
  showOfficeDataToForm: function (paramRequestData) {

    $("#inp-city").val(paramRequestData.city);
    $("#inp-phone").val(paramRequestData.phone);
    $("#inp-address-line").val(paramRequestData.addressLine);
    $("#inp-state").val(paramRequestData.state);
    $("#inp-country").val(paramRequestData.country);
    $("#inp-territory").val(paramRequestData.territory);
  },

};