package com.shoppizza365.export;

import java.util.*;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.shoppizza365.model.*;

public class ExcelExporter {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<CCustomer> customers;

	/**
	 * Constructor khởi tạo server export danh sách customer
	 * 
	 * @param orders
	 */
	public ExcelExporter(List<CCustomer> customers) {
		this.customers = customers;
		workbook = new XSSFWorkbook();
	}

	/**
	 * Tạo các ô cho excel file.
	 * 
	 * @param row
	 * @param columnCount
	 * @param value
	 * @param style
	 */
	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else if (value instanceof Long) {
			cell.setCellValue((Long) value);
		} else if (value instanceof Date) {
			cell.setCellValue((Date) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	/**
	 * Khai báo cho sheet và các dòng đầu tiên
	 */
	private void writeHeaderLine() {
		sheet = workbook.createSheet("Customers");

		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(row, 0, "ID", style);
		createCell(row, 1, "Firstname", style);
		createCell(row, 2, "Lastname", style);
		createCell(row, 3, "PhoneNumber", style);
		createCell(row, 4, "City", style);
		createCell(row, 5, "Country", style);
		createCell(row, 6, "CreditLimit", style);
		createCell(row, 7, "PostalCode", style);
		createCell(row, 8, "SalesRepEmployeeNumber", style);
		createCell(row, 9, "State", style);

	}

	/**
	 * fill dữ liệu cho các dòng tiếp theo.
	 */
	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (CCustomer customer : this.customers) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			createCell(row, columnCount++, customer.getId(), style);
			createCell(row, columnCount++, customer.getFirstName(), style);
			createCell(row, columnCount++, customer.getLastName() , style);
			createCell(row, columnCount++, customer.getPhoneNumber(), style);
			createCell(row, columnCount++, customer.getCity(), style);
			createCell(row, columnCount++, customer.getCountry(), style);
			createCell(row, columnCount++, customer.getCreditLimit(), style);
			createCell(row, columnCount++, customer.getPostalCode(), style);
			createCell(row, columnCount++, customer.getSalesRepEmployeeNumber(), style);
			createCell(row, columnCount++, customer.getState(), style);
		}
	}

	/**
	 * xuất dữ liệu ra dạng file
	 * 
	 * @param response
	 * @throws IOException
	 */
	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}
