package com.shoppizza365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopPizza365BackdendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopPizza365BackdendApplication.class, args);
	}

}
